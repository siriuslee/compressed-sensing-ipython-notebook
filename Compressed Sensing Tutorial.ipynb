{
 "metadata": {
  "name": ""
 },
 "nbformat": 3,
 "nbformat_minor": 0,
 "worksheets": [
  {
   "cells": [
    {
     "cell_type": "heading",
     "level": 1,
     "metadata": {},
     "source": [
      "Compressed Sensing"
     ]
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Introduction"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "<div style=\"font-style:italic\">a signal processing technique for efficiently acquiring and reconstructing a signal, by finding solutions to underdetermined linear systems. This takes advantage of the signal's sparseness or compressibility in some domain, allowing the entire signal to be determined from relatively few measurements.</div>\n",
      "<div style=\"text-align:right\"><a href='http://en.wikipedia.org/wiki/Compressed_sensing' style=\"font-style:italic\">Wikipedia</a></div>"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "It provides an elegant and simple solution to a complex problem. Say you have a satellite up in space with some fancy sensor picking up very high dimensional data. It can be quite costly to send all that data back from space, so wouldn't it be nice if you could just send back some lower dimensional version that could then be used to reconstruct the original signal? Of course it would. That would sure save a lot of bandwidth and computing on the ground is much cheaper than in space. So then, what's the best way to do this? As it so happens, it was shown by Candes et al[1] that if a signal is sparse, you can compress it by taking a small number of <span style=\"font-style:italic\">random</span> projections without loss of information. Super fancy, indeed."
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "<div style='margin-left: 2em; margin-right: 2em; text-align: justify'><img src=\"files/cs_tutorial/ganguli_sompolinsky_figure1.png\">Framework of compressed sensing (CS). A high-dimensional signal $u^0$ is sparse in a basis given by the columns of a matrix $C$ so that $u^0 =Cs^0$ , where $s^0$ is a sparse coefficient vector. Through a set of measurements given by the rows of $B$, $u^0$ is compressed to a low-dimensional space of measurements $x$. If the measurements are incoherent with respect to the sparsity basis, then L1 minimization can recover a good estimate $s^0$ of the sparse coefficients $s^0$ from $x$, and then an estimate of $u^0$ can be recovered by expanding in the basis $C$. [2]</div>"
     ]
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Details"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Here are the components:\n",
      "<ul>\n",
      "    <li>$u^0$ - a signal that has been measured and is assumed to have <i>sparse underlying structure</i>. $u^0 \\in \\mathbb{R}^n$</li>\n",
      "    <li>$s^0$ - a <i>sparse representation</i> of $u^0$ with at most $k$ nonzero entries. It is sparse in the basis $C$, yielding $y^0 = Cs^0$</li>\n",
      "    <li>$x$ - a dense, low-dimensional representation of $u^0$. $x \\in \\mathbb{R}^m, k \\lt m \\lt\\lt n$</li>\n",
      "    <li>$B$ - a random matrix, commonly N(0, 1 / m). $B \\in \\mathbb{R}^{m \\times n}$.\n",
      "        <ul>\n",
      "            <li>It matters most that $B$ is <i>incoherent</i> with $C$</li>\n",
      "        </ul>\n",
      "    </li>\n",
      "</ul>"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "As I see it, there are two really important uses for compressed sensing that have emerged thus far."
     ]
    },
    {
     "cell_type": "heading",
     "level": 3,
     "metadata": {},
     "source": [
      "1. Signal reconstruction"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "The theorems of compressed sensing state that if the components have their assigned properties (namely, $u^0$ has underlying sparse structure and $B$ is incoherent with that sparse dictionary $C$, then it is possible to recover almost exactly the sparse vector $s^0$. This holds true only when we provide a sparse dictionary $C$. If we do not have $C$, then it cannot be strictly determined but can be computed up to scaling and permutation of columns[3]."
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Reconstruction follows these basic steps:\n",
      "<ol>\n",
      "    <li>Initialize a dictionary $C$ and random projection matrix $B$\n",
      "        <ul>\n",
      "            <li>$C$ can be anything known to provide a sparse basis for the data in question. In images or movies you might use gabor wavelets, whereas in sounds you might use gammatones</li>\n",
      "        </ul>\n",
      "    </li>    \n",
      "    <li>Given a signal $u^0$, compute $x = B u^0$, where $x$ is a low-dimensional projection</li>\n",
      "    <li>Infer $\\hat{s} = \\operatorname*{arg\\,max}_s \\frac{1}{2T}\\left|\\left|x - BCs\\right|\\right|_2^2 + \\lambda|s|_1$\n",
      "        <ul>\n",
      "            <li>This can be done using any LASSO algorithm, of which there are many (e.g. <a href='http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Lasso.html'>sklearn.linear_model.Lasso</a>)</li>\n",
      "        </ul>\n",
      "    </li>\n",
      "    <li>Compute $\\hat{u} = C\\hat{s}$, the reconstruction of $u^0$.</li>\n",
      "</ol> "
     ]
    },
    {
     "cell_type": "heading",
     "level": 3,
     "metadata": {},
     "source": [
      "2. Low-dimensional classification or regression"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "The other nifty thing about compressed sensing is that, without knowing anything about the sparse structure underlying the data, other than that it exists, you can embed the data in a low-dimensional ($m = O(k \\log n/k)$)space without losing much information. At these levels of compression, pairwise distances are still conserved between datapoints."
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Classification or regression follows these basic steps:\n",
      "<ol>\n",
      "    <li>Initialize a random projection matrix $B$</li>\n",
      "    <li>Given a signal $u^0$, compute $x = B u^0$, where $x$ is a low-dimensional projection</li>\n",
      "    <li>Build regression model or classification model on low-dimensional data $x$</li>\n",
      "    <li>Win</li>\n",
      "</ol>"
     ]
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Examples"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "from cs_tutorial.simple_example import *"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [],
     "prompt_number": 1
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "ic = SimpleIC()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [],
     "prompt_number": 2
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "ic.plot_all()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "Plotting 2 independent components\n",
        "Plotting 2 principal components\n",
        "Plotting 2 random projections"
       ]
      },
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "\n"
       ]
      },
      {
       "metadata": {},
       "output_type": "display_data",
       "png": "iVBORw0KGgoAAAANSUhEUgAAAXoAAAEHCAYAAACgHI2PAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAIABJREFUeJztnXlYFFfW/78gGEWEBmRRGwFZBGTrDIrGJOJgq9GIvG+W\nMWbBjMo8mTgzJnmM5snkp5N3VIyT8Y0mGc28JtEYspiMSyIyo1E00aij4jJqxBhQZFNZBDQKyP39\nca1eoPeurq7uPp/n6aeru27dutXn9qlb5557jhdjjIEgCIJwW7yd3QCCIAjCsZCiJwiCcHNI0RME\nQbg5pOgJgiDcHFL0BEEQbg4peoIgCDfHLRW9v78/+vfvj/79+8Pb2xt+fn7o378/AgIC8Mknn4h6\nrsWLF+Ppp5/WfGaMYdWqVUhNTYW/vz8iIyPx+OOP4z//+Y+o5/U0pJapr68v+vfvj6CgIIwZMwYH\nDx7U7K+trcWsWbMwaNAgBAQEICkpCYsXL8bNmzdFbYen097ejlmzZiE6OhoBAQFQqVQoKSkxWv7D\nDz9Er1690L9/fwQGBiItLQ2bN2/W7K+srIS3t7emH8XExGD58uVSXIrTcUtF39bWhtbWVrS2tiIq\nKgpff/01Wltb0dLSgieeeELUc3l5eel9/sMf/oBVq1Zh9erVaGpqQnl5OfLy8rB9+3ZRz+tpSC3T\nJ554Aq2trbh69Sruv/9+/Pd//zcAoLGxEaNHj8bt27dx8OBBtLS0YOfOnbh+/TouXLggajs8nc7O\nTgwZMgT79u1DS0sL/vznP+Pxxx/HxYsXjR4zZswYtLa2orm5GXPnzsWMGTPQ1NSkV+b69etobW3F\nJ598gtdffx3//Oc/HX0pTsctFb0hbt26hb59+6KxsREAsGTJEvj6+qKtrQ0A8Nprr+GFF14AwDvC\nM888g7CwMERHR2PJkiUwtq5M9/vz58/j3Xffxaeffors7Gz4+vqib9++mDFjBhYsWAAAKC4uxvDh\nwxEQEAClUok333zTkZft1jhSpsI+Hx8fPPPMM6irq0NDQwP++te/IjAwEBs3bsSQIUMAAEqlEitX\nrkRqaioA4IUXXkB4eLhmVHn69GmH/g7uip+fHxYtWqT5nadMmYKYmBgcO3bM6DGC3Ly8vPDUU0/h\n9u3bRm/Ao0aNwvDhw3H69Gk0NDTg4YcfRlBQEEJCQvDggw8a7R+uiMco+j59+mDkyJEoLS0FAOzd\nuxfR0dH47rvvNJ+zs7MBAL/73e/Q2tqKiooK7N27Fxs2bMAHH3xg9hzffPMNIiMjkZmZabTMrFmz\n8N5776GlpQWnT5/GL3/5S7uvzVORQqa3b9/Ghx9+iCFDhiAkJAS7du3SjO4N8c9//hPffvstzp8/\nj+vXr2PTpk0ICQmx+1oJoL6+HuXl5Rg+fLjZsnfu3MEHH3wAhUKBYcOG6e0TbuT79+/H6dOnoVKp\n8Je//AWRkZG4du0arly5gmXLlvV4WndlPEbRA8DYsWOxd+9e3LlzB6dOncLvf/977N27F7du3cKR\nI0fw4IMP4s6dO/jss8+wbNky9OvXD1FRUXjppZfw0Ucfma2/oaEBERERJsv07t0bp0+fRktLCwID\nA6FSqcS6PI/EUTL9/PPPERQUhCFDhqCsrExj621sbMTAgQONHte7d2+0trbi7Nmz6OrqwrBhw8z2\nCcI8HR0dePLJJzFz5kwkJCQYLXfw4EEEBQWhb9++mD9/Pr766iv0799fr8yAAQMQEhKCOXPmYPny\n5Rg3bhx69+6N2tpaVFZWolevXhgzZoyjL0lSPE7Rl5aW4tixY0hNTcX48eOxd+9eHDp0CHFxcQgK\nCsK1a9fQ0dGBqKgozXFDhgxBdXW12fpDQkJQW1trssyXX36J4uJiREdHIzs7W2+Sj7AeR8n0V7/6\nFZqamlBfX49du3ZpbsghISGoqakxety4ceMwd+5cPP/88wgPD8dvfvMbtLa2infBHkhXVxeefvpp\n9OnTB2+//bbJsqNGjUJTUxOampqQm5trcLK1oaEBjY2NOHPmDObOnQsAmD9/PuLi4jBhwgTExsa6\n3SStRyn60aNH49y5c9i8eTOys7ORlJSES5cuobi4WPOIP2DAAPj6+qKyslJz3KVLl6BUKg3Wqft4\nl5OTg8uXL+Po0aNG25CZmYktW7bg6tWryMvLw+OPPy7KtXkqjpKpMfvs+PHjsXnzZpP229/97nc4\ncuQIzpw5g/LycqxYscLm6/N0GGOYNWsWrl69ii+//BK9evWy6Lh+/frhb3/7G/bu3Yu9e/eaLe/v\n74+//OUvuHDhArZt24a//vWv2L17t73Nlw0epej9/Pzwi1/8Au+88w7Gjh0LALjvvvuwZs0azede\nvXrh8ccfx6uvvoq2tjZcvHgRK1euxFNPPWWwTt0/fHx8PH7729/iiSeewN69e9He3o5bt27h008/\nxfLly9HR0YGPP/4Y169f17iBWdpxCcM4WqbdefHFF9HS0oL8/HxcunQJAFBdXY2XXnoJp06dwpEj\nR3Do0CF0dHTAz88Pffr0IRnbwXPPPYcffvgB27Ztwz333GPVsUFBQSgoKEBhYaHZstu3b8ePP/4I\nxhgCAgLQq1cvt5KbRyl6gD/qd3Z2YuTIkZrPbW1tePDBBzVlVq9ejX79+mHo0KF44IEH8OSTT+LZ\nZ581WJ+Xl5feqH7VqlWaR/egoCDExcVh69atyM3NBQBs3LgRMTExCAwMxHvvvYePP/7YgVfrGTha\nproEBQXhwIED8PX1RVZWFgICAjB+/HgoFArExcWhpaUFBQUFCA4ORnR0NAYMGID58+eLf9EewMWL\nF/Hee+/hxIkTiIiI0Pi/G1s3YUhu8+bNw549e3Dy5ElNGUOcP38earUa/fv3x3333Yfnn39eM1Bw\nB7woHj1BEIR7Y3ZE/+tf/xrh4eEaH2FD/P73v0d8fDzS09NRVlYmagMJx1BSUoLExETEx8cbnXgi\nucoXe+RnybGEm8HMsG/fPnbs2DGWkpJicP/27dvZQw89xBhj7ODBgywrK8tclYST6ezsZLGxsayi\nooK1t7ez9PR0dubMGb0yJFf5Yo/8LDmWcD/MjugfeOABBAUFGd2/bds25OfnAwCysrLQ3NyM+vp6\n8e5EhOgcPnwYcXFxiI6Ohq+vL6ZPn46tW7fqlSG5yhdb5VdXV2fRsYT7YfdkbHV1NSIjIzWflUol\nLl++bG+1hAMxJLPuPuUkV/liq/yqq6tRU1Nj9ljC/fARoxLWbT7X0My2Oy0ndnW++OILi8qRXOXN\n//3f/+l9fvvtt/Vk1l1+1kBylRf2yBIQYUQ/ePBgVFVVaT5fvnwZgwcPNliW3Y0xIcZr0aJFsqxL\n7vUZkllVVVWPxUPuINc5cxiiohbhoYcYmprk1TZ76vv+++8xceJEzeelS5f28BU3JD+lUmmR7MWS\nq72/1733LsLYscwu+dnbBmcfz5g4TpF2K/rc3Fxs2LABAI8zoVAoEB4ebnfDCMeRmZmJ8+fPo7Ky\nEu3t7fjss880fv4C7iDXr78GLl4EduwAjLjMuyT2yM+SY+VAYiJw7Biwd6/7yc8ZmDXdCKs8r127\nhsjISPzpT39CR0cHAOA3v/kNJk+ejOLiYsTFxaFfv34WRQQknIuPjw/efvttTJw4EXfu3MGsWbOQ\nlJSEtWvXAnAPuSYmArphh9rbndcWsTEmPwBYu3atSfmZOlZO1NXpfxZpYOu5MIkAwB56iLGmJnHq\n27NnjzgViVyX3OsTW+Ri1yfWtQYGMsbVwx4GMJaXZ3+dcpYrY+LKQoy67Lm+AQO0suvXj7HKSunb\nIIfjGRNHFpKtjOWTOwyPPQZ8/rkUZyQMYSpglxzqE4vQUODaNe1nhQI4fhzQCWDpdogpC2fKtaAA\n2LIFuHpV+50n6w0xZCFprJsRI4D33pPyjISncuSI/ufmZuD++53TFsI6ysv1lTwA7NzJ51sI25BU\n0ffuDcyYwf90BOFIoqKA7h6CbW2kLFwBQ5n/6EZtH5Iq+v37+Qx6QYGUZyU8jYIC4G4oej1IWbgG\nxsxrn34qbTvcCcnDFJP5hnA05eXcLa+7WdPPD7ibTpaQMXfD/Pdg/HiyBtiKpIr+sceAf/2LT4wR\nhKP46SfD3585496Tse5AQYG+W6wut26RNcBWJPW6kaN3hqfhCV4399/PzYS6jBsHuFFmOIO4g9fN\noEHGFf2IEZ45UHQ5rxuCkIKAgJ7f+flJ3w7CehoaDH/fp49nKnmxIEVPuB1FRdxMGBio/c7HRztJ\nO3ky2Xrliu7AVfCa8vYGDh8mJW8PpOgJt6KgAMjL466UGRn8O5UK+PBD7SQteX7JF3//nt91dQGv\nvCJ9W9wJUvSEW6GrzCsr+QrZkBC+TzDfkOeXfPnFL/i7SsVH8gKUydI+SNETboWuMv/5Z77Cctcu\nYOZMrUmHbL3yZdMmLqPdu7Wmt759gQMHnNsuV4cUPeFWhIbyV2AgcDfIKgBu71UoeLwUUvLyRVdG\nx44BSiVw9ix3i6U5FtshRU+4Ff/6l3YU36sX/87fn9vsm5tJWbgSUVFAVZV27QPNsdgOKXrCrdCN\nO9+3L7fztrVxxf/ss6Qs5Iyhm3BiIh/dh4Zqy9Eci/WIkjOWIORAYiJw/TrfHj6chynu6tLuv3FD\na8Pv3x9oauIKhUw58mDjRj6vAgBPPcUzhNXVaWV6/jy337/3HsnMWmhlrIfhzitjFQqtUhg0iCuN\npibt/oEDeRiEhARtGNyICG4DdgfF4eorY319gc5Ovj1oEFBdrc0r4OfnuSEsaGUsQejg68vf/fy4\nl4bgqiegUnGF3qeP9ru6OjLhyAXBy0aQHwCo1Ty8+YgRfD/NsdiGpIqehEM4ioICIDaWK/GDB/nI\nb9Mm4J57+P7+/YF33+XbQ4Zoj/P1BVaskL69hD4FBXw+xcsLuPderdLfs4fPu+zdCwwbxkf1NMdi\nPZIqehIO4Si++go4dIhHOHztNf7dyy9rFX1rKzB/Pt/WjYXT0aH9nnAeX33FzWmM8VDSgp64ckVb\n5soVbqcHaELWWiRV9L16cWHRqJ4QG11vGyFGSnk50NLCt3VH7kVF3DYvsGULcOqUNO0kDKM7lxIQ\noFXi3t001IgRtOjNFiRV9Hfu8McuGtUTYnPvvfxdpQI++IBv60as7OgA5s3j2woFn4AVbggdHUBW\nlnRtJfQpKNC/UY8apVXiwloIgHtVbdzI9+XlkSnYGiSfjA0MpEcuQnx0l84LSqKoSD9vrK4yUSh4\nREuAjxoPHZKurYQ+5eX6USt9dJy+hcB0ALfRKxTc7VKw0ycmkrK3BMkV/f330yMXIT6GwhsoFPqK\nXnDdEzh6lC+qOn4cSE2Vpp1i0NjYCLVajYSEBEyYMAHNRjRdSUkJEhMTER8fj+XLl2u+X7x4MZRK\nJVQqFVQqFUpKSqRqukG65wro3Vu7HRzM3/v35+sgmpv5PIxAfT1fCEeYRlJFr1LxRy+CkArdkeLJ\nk/r7UlOBmzddS8kDQGFhIdRqNcrLy5GTk4PCwsIeZe7cuYO5c+eipKQEZ86cwSeffIKzZ88C4H7Z\nL774IsrKylBWVoZJkyZJfQl6hIZqXWNTU7WmNwAIC+P7Wlv56uaCgp5uszJZxiFrJFX0uo/VBCEF\nukogKcl57RCTbdu2IT8/HwCQn5+PLVu29Chz+PBhxMXFITo6Gr6+vpg+fTq2bt2q2S+XRW4AcPGi\nNgBdbKy+jqis1O4LCuJm302b+A0A0OYaIEwjaQgEUvKE1PTqxZ0AAODcOcNlCgq4ndjPj9v15d5P\n6+vrER4eDgAIDw9HfX19jzLV1dWIjIzUfFYqlTikMxGxevVqbNiwAZmZmXjzzTehMHDRixcv1mxn\nZ2cjOztbvIvQ4cgR/u7jA7z+uv4+wazj4wOkpfFthQKYNInb6IVcA+5EaWkpSktLRa2TQiB4GO4c\nAsEQAwbwPKSmltBnZ/PJPYBP6H7+uaRNNIharUZdXV2P75csWYL8/Hw06fgjBgcHo7GxUU8WX375\nJUpKSvD3v/8dALBx40YcOnQIq1evxpUrVxB6N0rYa6+9htraWqxbt07vPFLKNTBQ6warVPKIlQLN\nzfohKwT5DBzIVzUDfHR/7pz8b9C2IoYsKKgZ4dYEBgKNjdqFU4aQY+apnTt3Gt0XHh6Ouro6RERE\noLa2FmGCHUOHwYMHo0pHY1ZVVUGpVAKAXvnZs2dj6tSpIrbcegRvKG9voLhYf59CwedRAD6qFxbD\n6XpQXbnCn8rkcIOWKxTrhnBrGhq4nb6pybgrnqtlnsrNzcX69esBAOvXr0deXl6PMpmZmTh//jwq\nKyvR3t6Ozz77DLm5uQCA2tpaTbnNmzcj1Ymz0QUF2onYri7gf/6nZxlBqXd2cpMNoF03AXA7vVxu\n0LKFSQQANnYsYw89xFhTk1RnJbojtsgl7EI2MWAAY1zV89djjxkvO2cOc4k+2tDQwHJyclh8fDxT\nq9Ws6W5jAbDJkydryhUXF7OEhAQWGxvLli5dqvn+6aefZqmpqSwtLY1NmzaN1dXV9TiHVHIdOFAr\nG4XC8O/u5aUtM2EC/66pibG8PMamTZO3rMRADFlIaqMH+KnkYgf1RNzNRm9uInXGDOCzz/hoUaUy\n7fklR1u9NbhimOLgYG34gylT+GKo7vTurfW8mTwZ2L7d4c2SFS4ZplhOdlDC9TGXMWrLFm3ykdBQ\n06aZCxf4e2AgRbSUCmF+JCAAeOcd8+V1F8ARliOponclOyjhGui65gkTdbroJgj/z39M1yV45Fy/\nThEtpSI6mr+3tBj/zf39tdtHjlDIA1uQVNF3X6JOEPYiBL3q7OSP9d0Roh96eQHmVvoL4Yv9/bVp\nBgnHIvzmpp70dVfC1tf3fHKjZCTmIa8bwqUR4qL4+fE45t0RvDMYM+zRoUtRETfvCMnEKcqq47HE\n42nTJh6TCDBsVqOE7+Yxq+iNBUYSuHbtGiZNmoSMjAykpKTgQ1qPLHssDYr161//GuHh4U51vzPH\nkSN8kY2xxVBBQfzdkrkhhQLIzLS8PGEfiYncdLNnjzbXryFeflnrgmnIrCbHdRCyw5RLTmdnJ4uN\njWUVFRWsvb2dpaenszNnzuiVWbRoEVu4cCFjjLGrV6+y4OBg1tHRYdBFyBVc19wdAGz+/Pls+fLl\njDHGCgsL2YIFCwyW3bdvHzt27BhLSUkxWZ+caWpizN+fsYAA7mpZWWm+/GOPuWYfFVMWUsg1MFDr\nNqlUGi83dqy2XFBQT9m4sswsQQxZmBzRmwuMBAADBw5Ey931yy0tLQgJCYGPj+EFt8LjFYUVdS6W\nBMUCgAceeABBwpBYhlhim1UogNu3+WTftWvAffeZrtNQuGPCMegmczdkdhMwFO9GF5KZeUyGQDAX\nGAkA5syZg1/+8pcYNGgQWltb8blJ5+PFAHh2n9JSxwVJIrQYCpBkSVAsa5Aq+JUuwpJ34ZHf0iXw\nKpVj2yUljgh+JSXjxwP/+Ac3uQjJwA0hpH68fZsPFmfO5G6zhOWYVPReFjitLl26FBkZGSgtLcWF\nCxegVqtx4sQJ9O/f30DpxWYXrRDiYCwoVne8vLwskrMpdBW9VJSXa5W8j49pv/cxY4DSUj4atCQf\ngqtEs+x+U/3Tn/7kvMbYQG0tD28gpBc1dqNWKIB+/biiB4z70ruK3JyBSUVvKjCSwIEDB/Dqq68C\nAGJjYxETE4Nz584hU5jV0uGxx/hkCQnA8RgLiuXl5WVRUCy5IyxuArhr5fz5xhVFVBT3prH0MgUv\nDoCCZTkSayZR+/Th7wEBwP/+r+EyJDfjmLTRmwqMJJCYmIhdu3YB4CaBc+fOYejQoQbra2sTqdWE\nXVgSFEvu6HrYCAkpjFFZycPcWuoySV4c0mBNMDkhDWRLC/CHPxguQyubTWButtZQYKQ1a9awNWvW\nMMa4p83DDz/M0tLSWEpKCvv444+NzhybCypFOB4ARoNiVVdX6wXFmj59Ohs4cCDr3bs3UyqV7P33\n3zdYnzN46CGtF4Y5TxrdoFg7dpiv21W9OMSUhaPlam0Aud69tTKcMsVwmTFjLAte52qIIQtJg5oF\nBjKcOGHY35mQBncJapafzz240tP5ghpTI0Jdm663tzbjlDlczebrSkHNdBOHTJtmfnJVN/hZXh6w\neXPPMpMn8z4xYoR7hVpxuaBm168Do0bRMmXCPgoKgK1brTPHCGRlWV5240atS/CTT1rfTsI4uolD\nLPEFEMIgqFT6ycN1KSoCYmL4aukZM0jP6CJ5CIS6OlqmTNjH119rPW4UCvN2dG+dXn7xouXn0Q2I\ndvy45ccR5hFCU5hS3Lps2sTt+aY89hQKYMgQYP9+CofQHckVfWoqTXAR9nHrlnZ7zBjzj+jCmi8/\nP+DAAcvPI/h2W3scYZqCAuDUKT7yNuU/r4tCwV95eaYXyNFEumEkV/TXrrmP7YxwDrqP8Zb4xR89\najoejrnjcnP5nABFRxSH8nIehbK9na9vsHTk/fXX5lfXu1paSKmQPDm4bq5HgrCFTZu4crB0TUZU\nFKCzHMRihON0Jw5pVab9CKNuwLp8rw0N2m1dG78uQjgEQh9JR/TC6jYaFRH28PLLwJUr0k24WTtx\nSJimqIh72uTlWbdKvl8/7bYQnpqwDMoZ62G4g3ulLbld7XGVVKu5d4/cw3e4knulLbiKHMTG5dwr\nAZokIezHlgk3e5JTWOLxQVhGQQEwaBD3i1errXsis1QOlHGqJ5QzlnApCgr4MviICPMLpXSxxxtD\nsPu+/DIpEHspL+fBzJqarF8DYanJjjJO9URSRU+xbgh7EAJV7d/PJ0etSeAdGspflrrzGYIUiP3Y\nOhELWOZ1o3sOsh5okVTR79gBJCXRaIiwDd3QxOYCmXXn4kXbVtLqIgcFYm8aSEuPdxRFRXwSdto0\n601hul43+/cb1yPkYtkTWhlLuAyCog0KAsrKrPsTixHZUA4KpLCwEGq1GuXl5cjJyUFhYaHBcs8+\n+yxKSkpsPt5RKBQ8Ts2WLdb/hrorla9e5a6uxs5BGae6YXdYNAvB3eiVKpXrRQV0J8QWuYRdiDU1\nMRYTw6MUWpt72F0iGw4bNozV1dUxxhirra1lw4YN0+zrLouKiooe+X5NHa+LlHK1FB8frQxNRbEU\nsDZCplwRQxaSL5iKiqI7LWEbQiwTW5JLXLrE3109Vrm9aSCtOd4ZKSJNERiob74xkppaw8aNwM8/\n8+0nnwS2b3dc28TEESkiJVX0I0ZYFsCIIIxhq5386lX+fv068Nvf2vend3T4YmNpIJcsWaL32d40\nkOaOd0aKSFMcPQoMG8YXXapUwIcfmi7vqkHpHJEiUlJFT5MjhK0UFHCvi59/5ikBrXGtBLQZigD7\n//SOTllnLA0kALvTQLpyGsmoKO0cnyXhL4QnAApKJ/FkLCl5wlYE/+vmZu5LbY1rJSBuJEphYrdX\nL94WKR1X7E0D6aw0kvYslNLFmolWW4PZuSUizBVYhISnIkwgthykkquQOtDWCf3KSsaUSvNpBy1B\nd2JX6sldU2kgdWVhLA2kseO7I7ZcIyKc95u5OmLIQtJYNxKdijCBq8a6aW7mi2QY47ZZa58OFQq+\nYM/bm4/0urmXW4WQsg4AAgKAkyflMWKUc6ybkBCgsZFvKxRARYV0T/iulhKyO2LIghS9h+Gqit5e\nfHy0uWK9vbnt1tY/fHMzEBen9QCRS5A+OSt6ISCZQsHnSKS8MVqbn1ZuuGRQM4KwBXsDVXV16W+b\nWkJvDoUCGDmSb/v787gttNrbNEJAsooK+5S8Lf2AwkxLrOgpGBRhK/bGmRGyUgnYO1gtKuKxc9ra\n7Aur4CmIlUPAln5gbX5ad0TyWDf0hyBswd44M7oulX5+wFtv2dcehQLIzLSvTZ6EWAHhbOkHFGZa\nYhu9jw/DsWP2TYQR9uGqNvrmZuvSB3anVy99840YdnV72yQ2crXRFxQAX37JJ2PtTRoit99cClxu\nMhZgUCpty99JiIOrKnp78fbWmmuGDQMOHnQ/RSFXRe/npw1FMGEC8M9/2lefq3vRWIsYspB0Zay3\nN1BcLOUZCYITEgJcu8a3BTc/Qhpu3dJunzplf31ffWVbsnZPu0HoIqmNvqsL+J//kfKMBMHRNdtc\nvWqf1w1hHb16abdVKvvrs9WLxpMTx0iq6GnSinAWgueFgAtYm9yGAQP4e//+wLvv2l+frV40ckgc\n4ywkVfQU1IywFrESPW/aBPTpw7f797ff60YXSkZtmthY/t7aan2MIkMMGsRdW0NCrDtODoljnAUF\nNSNkzVdfaR+3jWUUsgSFQutLL5bCEfBkk4A5Cgp4UDHA+hyxxrA1LaQnZ56ilbGErBFzVWNAAH8X\n+9Hdk00C5igv5yuHAZ40Rgwle+QIf/fxAV57zf76PAFaGUvIGjFXNTrq0d2TTQLm0L0JmksUYinC\n5G5nJ9cphHloZSwha8Rc1SjWMvzuKBT8lZdHg5nuOOIm2Ls3f/fzA777zrpjPXU+hbxuCNlSUMCV\nZ1ubOPU50pauW3dSkmcpEWMUFPDf4ptvuLIX6zc5csT2hCKeOp9CXjeEbBH7TylkhnJEgnDBRAHw\nxTz2TBy7C+Xl/LdobBQ38FtUFF9db0sUTE+dTzGr6EtKSpCYmIj4+HgsX77cYJnS0lKoVCqkpKSY\nzBQv9iMzYRuNjY1Qq9VISEjAhAkT0GxAKFVVVRg3bhyGDx+OlJQUrFq1SvJ2iv2nFBTD9eviet0A\n3ETh66v97KnhcHURbqwAj28llmItKOB9w9eX++hfvGj5sUVFQEwMN/94lD4ylX6qs7OTxcbGsoqK\nCtbe3s7S09PZmTNn9Mo0NTWx5ORkVlVVxRhj7OrVqwbrAkApxGQAADZ//ny2fPlyxhhjhYWFbMGC\nBT3K1dbWsrKyMsYYY62trSwhIaGH7IX6HEVTE+8v1qYNNIaQjnDECPHq1GX8eNtTHYqBmLIQoy7d\nlIt5eSI06i5jx+qnJVQqbT8+IsI5srIGMWRhckR/+PBhxMXFITo6Gr6+vpg+fTq2bt2qV6aoqAiP\nPPIIlEoAfY/oAAAgAElEQVQlAGCAsAzOAJ72uCRXtm3bhvz8fABAfn4+thgIFhIREYGMjAwAgL+/\nP5KSklBTUyNpO8X2e3a0d8ymTXy06OfnYaNFI+i6s4oZB17XTAYAn35q+/GeYmYzqeirq6sRGRmp\n+axUKlFdXa1X5vz582hsbMS4ceOQmZmJjz76yGh9gYF2tpYQhfr6eoSHhwMAwsPDUV9fb7J8ZWUl\nysrKkJWVJUXzHIajF8woFNxXfP9+z5vs605BAdDSAkRE8Bug2O6sujz+uPXHe5qZzWT0Si8LfoGO\njg4cO3YM33zzDW7evInRo0dj1KhRiI+P71F2167FGDOGj6qys7NN2vMJ+1Cr1airq8ONGzdw48YN\no+W8vLxMyrmtrQ2PPvoo3nrrLfj7+xsss3jxYs22p8tVysm+0tJSlJaWOvYkNvL110BtLd8eORI4\nd048Za9Q8MVSnZ38890HT6uOHzuWTxB7TNYpU3ad77//nk2cOFHzeenSpaywsFCvTGFhIVu0aJHm\n86xZs9imTZt61AXAYbZRwnIAsGHDhrHa2lrGGGM1NTVs2LBhBsu2t7ezCRMmsJUrV5qsj9Ai9ryC\nNYgpC3vrCgrSt6OLPTfXty+v19ubsZMnrT++qYmxmBg+j/DQQ/LWS2LI1aTpJjMzE+fPn0dlZSXa\n29vx2WefITc3V6/MtGnT8N133+HOnTu4efMmDh06hOTkZIP1kXulPMjNzcX69esBAOvXr0deXl6P\nMowxzJo1C8nJyZg3b57UTXTZhS2eHE9FF107uJgeNwJCjPuuLp7MxFo8zsxm7k5QXFzMEhISWGxs\nLFu6dCljjLE1a9awNWvWaMqsWLGCJScns5SUFPbWW2857K5E2A8A1tDQwHJyclh8fDxTq9Ws6e5w\nprq6mk2ePJkxxti3337LvLy8WHp6OsvIyGAZGRlsx44dButzBLqeEeSppU9DQwMbP358D/l1l8Wz\nzz7LwsLCWEpKit73ixYtYoMHD3aoXB3lcSOg+7QQFmZbHY72whILMf5jkmlfALJ/RPIExFbMjlD0\nc+YwFhzsXFdFOWPMPba7LPbt28eOHTvWQ9EvXryYvfnmmybPYa9cHa1EdRX9uHG21eFMM5s1iPEf\no1g3hOwoL9em+4uKcl0ziKPMT5a4xwLAAw88gKCgIIP7mIMzrzjalVU3a9W5c7bV4UlmNklzxpIf\nPWEJup4rruwRIYRwALjS//xzceq11j3WEKtXr8aGDRuQmZmJN998EwoD2s4ebypBiToKHx/gzh2e\nh7qkxHHncQaO8KbyYo6+tQsn8vJCUxPziLunnBEjo7wj6wP46LeggA8KXLm/TJ7Mn2JHjLB+ZCu4\nx3ZnyZIlyM/PR5MQ5B1AcHAwGhsbDcqisrISU6dOxSmdrNxXrlxBaGgoAOC1115DbW0t1q1bp3ec\nPXKVIgn3qFHAoUN8+7HHHHtTcTZi/MckHdG78p+WkA7dcMKOUhRSUFRk+w1r586dRveFh4ejrq4O\nERERqK2tRVhYmFV165afPXs2pk6dal3jzOCoJxldgoP5O1kJLIMyTBGyw11CyTrKBmyJe6wpaoWV\nTAA2b96M1NRUUdsnxaIxjw1OZiOk6AnZ4amhZC1l4cKF2LlzJxISErB7924sXLhQs2/KlCma7See\neAL33XcfysvLERkZiQ/uTngsWLAAaWlpSE9Px969e7Fy5UpR2xcayl+ODHkiph+8q67ZsAZJbfQS\nnYowgdxt9EIy6QsXgIMHbYs57qmIKQt76srO1ppuHGk/t2cORBep2msrYsiVcsYSsqK8nI/S6urE\njxnvLDxhxKiLVE9kYrlwesITJPnRE7LCHf907jLnYClhYY433QDi5QD2hOTulDOWkBXu+Kdzx5uX\nMQoKgC1bgKtXxU0faAixbqCesHCK/Og9DLnb6N2R5mbg3nuBQYN4Mg5HuYzKwUY/aJA2PLFCAVRU\nOE6BimWjlzsuZ6N3V0EQhCk8KVKiEFUSAMaMcex/XsynP3efRyH3SoKQAE8x3/ziF/xdpQI2bnTs\nucSy0QPuP49Cip4gJMBTFvhs2sRH2bt3O/4JXkzlLNyIvb2Bb74BLl60v31ygvzoPQw52+iliJHi\nTBztry0HG72UiGmjb27mnkJCekKlEqiqEqed9uJyNnqCMIYQE8WdH5/d3XwjtZ1bTBu9QqGtw88P\n+O47+9snJ2hE72HIdUSvO9oNCgJ++sn9RvSO9r5x9oh+4EC+0A0Apk3jbpauxMWLwP33cyUvpxXZ\nLjeid9cZbcJ+hNFuUBBQVuZ+Sh5wf+8bncjJGhOIKxEVxc01clLyYkErYwlZIDyG//STe/7RBNzZ\nfNO3r3bbR9IA6OLhrm6WkppuRoxgbr2wwRWQq+nGU3BkUhVnm27Uar4aVqWSxuvGEcgxwJkYcqWV\nsR4GKXr3xdmK3h0yg8lxta3LKfrgYIZ77+W+tnL4AT0RUvTywBGupM5W9O6AHG9WLqfoAX4quTwS\neSKk6OWBbkyY8HDghx/sVyyk6N0Tl/O6Abj9zt0moQjCWnRjwtTXk5MC4VgkVfQDBgAhIVKekSDk\niRATBgD69wdWrHBeWwj3R1JFf+2a42NUE4QrsGmTdtDT2uo+2bQIeSK56cYd/YcJwloUCmDkSL5N\n/wnC0Uiq6N0tcxBB2IM7ZtNyJ9xp8RTFuvEwyOvGfSGvG3GRy+Ipl/O6cZe7I0EQ7o87hauQVNG7\ncwhagpCCxsZGqNVqJCQkYMKECWg2MGqqqqrCuHHjMHz4cKSkpGDVqlVWHU9w3Mm0RpOxhNNwJxuo\nvVj6WxQWFkKtVqO8vBw5OTkoLCzsUcbX1xcrV67E6dOncfDgQbzzzjv44YcfLD6e4CgU3Fzj6koe\nAMAkAgB77DHGmpqkOiNhCLFFbk99Y8cyBvDXY4+J1yZXxNLfYtiwYayuro4xxlhtbS0bNmyYZp8x\nWUybNo3t2rXL7PG6SKgaCDOIIQtJg4lS2ANClwsX+HtgIC0YstQeXF9fj/DwcABAeHg46uvrTdZb\nWVmJsrIyZGVlWX384sWLNdvZ2dnIzs42fyGE3ZSWlqK0tFTUOsnrxsOQk9fN/ffzJBwAxT+Kjwcq\nK3nmqeRkNZqb63qUWbJkCfLz89Gkk+EjODgYjY2NAHrKoq2tDdnZ2fjjH/+IvLw8AEBQUJDR43Wh\n/6t8EEMWLpoegHAHLl7k7zSiB65e5VmZGhuBysqdRhNTh4eHo66uDhEREaitrUVYWJjBch0dHXjk\nkUfw1FNPaZS8NccTWtwhab3ZydiSkhIkJiYiPj4ey5cvN1ru3//+N3x8fPCPf/xD1AYS4mOJ58Wt\nW7eQlZWFjIwMJCcn45VXXhG9HUImqevXKQSAry9/N5eYOjc3F+vXrwcArF+/Xk+JCzDGMGvWLCQn\nJ2PevHlWH0/oU17uBh6Dpgz4nZ2dLDY2llVUVLD29naWnp7Ozpw5Y7DcuHHj2JQpU9gXX3xhsC4A\n7KGHaDLW2QBg8+fPZ8uXL2eMMVZYWMgWLFhgsOyNGzcYY4x1dHSwrKws9u233xqsz1YeeohPPo4Y\nQf2ispKxfv0YGzWKmfyfNDQ0sJycHBYfH8/UajVruluwurpaI4tvv/2WeXl5sfT0dJaRkcEyMjLY\njh07TB7fHXvk6m44u5+KIQuTNRw4cIBNnDhR83nZsmVs2bJlPcqtXLmSvfPOO2zmzJkmFT15Vzgf\nABZ7XgjcuHGDZWZmstOnTxusz1aamhh5Yumg63kTEWH97yKmciZFr8XZ/VQMWZg03VRXVyMyMlLz\nWalUorq6ukeZrVu34rnnngMgJBgxDPnQywNLPS+6urqQkZGB8PBwjBs3DsnJyaK2w638lEVA8LwB\ngLo64KmnnNcWQos79FOTk7GmlLbAvHnzUFhYqJkZZiZmhxsaFqOwEOjTh9y1HI1arUZdXR1u3LiB\nGzduGC3n5eVlVM7e3t44fvw4rl+/jokTJ6K0tNSgzMgNTxyKioDgYD6mB3hI7+Zm4wrGEW54hHFc\nelLW1HD/+++/1zPdLF26lBUWFuqViYmJYdHR0Sw6Opr5+/uzsLAwtnXrVoOPH2S6cT64a7qpra1l\njDFWU1Nj1nTDGGOvv/46W7FihcH6CPEICdGab6z9v4gpC5JrT5y1wE8MWZg03WRmZuL8+fOorKxE\ne3s7PvvsM+Tm5uqV+emnn1BRUYGKigo8+uij+Nvf/tajjAC50ckDSzwvrl27pvHG+fnnn7Fz506o\nVCpRzk+hD4xz9CjQqxffpv+LvHDlIGcmFb2Pjw/efvttTJw4EcnJyfjVr36FpKQkrF27FmvXrrX6\nZNevA3/4g81tJURi4cKF2LlzJxISErB7924sXLgQAFBTU4MpU6Zotn/5y18iIyMDWVlZmDp1KnJy\nckQ5v1u4qzmIqChg1Ci+ff0636aboTxw5SBnkq6MBRjy8oDNm6U4I2EIOayMnTyZK/kRI1zzT+No\nhN9HwNJVwxSP3rE4y0YvhiwkVfQqFcPu3fTHdiZyUPTNzfxP89571BcM0dwMJCVxzxtrboak6B2L\nsxKRuFziESEZMuHZvPwycOUKMGMGmSUMoVAAZ8+6rpnAXXFbG73Y7NpFNlmCbPSWoFDwV14eTVrL\nhaIiICYG6N3b9QYpkppuRoxgNEJxMs423SgUQEsLd1JLTubRK6k/GGbgQG6+AYBp04AtW0yXJ9ON\n43GG+cblTDek5Im2Nu2CoJ9+ov5givZ27bYFaxcJCXBV842kip7+1IS3t/b98GHntkXu3Hsvf1ep\ngA8+cG5bCI6rulhS4hEPw9mmm1OngKws4NAhIDVVtGa4JdZ6J5Hpxj1xOfdK6jjOx9mK3qXjhcgc\nUvTuicvZ6Ml7gCCPG+uhkBGEvUiq6OnPTbjqZJYzoZuj/HC1m6+kip7+3ISrTmY5E7o5yg/dm294\nuDb/sVyR1Ebf1MToz+1knG2jJ6zH0klZstFLR/d4REoljCZ0txeajCWsxlmKniZhHQ8peulobuYj\n+fZ23qfPnNEmuxcbl5uMJTwXsjMT7oRCATzyCHDPPcDIkTx3gJwhrxtCEsjObD+uNgHo7tTUALdv\nA6Wl8h+8UDx6D8NZphsKTWw/gwYBtbV829cXOH9e31xAphtpkSqvgkuabqjveCYKBQ8ARUredhoa\ntNsdHcD99zuvLYRreZBJquhVKuDDD6U8I0G4D337CluNANTw9U3AhAkTNLl9damqqsK4ceMwfPhw\npKSkYNWqVZp9ixcvhlKphEqlgkqlQklJiSTtdzdcafAiqaKn7FIEYTsjRvB3L69CvPCCGj/9VI6c\nnBwUFhb2KOvr64uVK1fi9OnTOHjwIN555x388MMPd4/3wosvvoiysjKUlZVh0qRJUl4G4QQoeiXh\ncGgSURw2beKmgri4bViwIB8AkJ+fjy0GAtVHREQgIyMDAODv74+kpCRUV1dr9pP93bPwkfJkCgWf\nRDpyxHE+p4T8+Ppr7STis8/SZLytCKaCoKB6hIeHAwDCw8NRX19v8rjKykqUlZUhKytL893q1aux\nYcMGZGZm4s0334TCwChs8eLFmu3s7GxkZ2eLch2EaUpLS1FaWipqnZJ73QCOXUVGmMYZXjfBwUBT\nE9+2JFMSAajVatQJ6aV0WLJkCfLz89Ek/KAAgoOD0djYaFAWbW1tyM7Oxh//+Efk5eUBAK5cuYLQ\n0FAAwGuvvYba2lqsW7dO7zjyupEPYshC0hE9APTqBXz3ndRnJZzJL37B8wXTZLzl7Ny50+i+8PBw\n1NXVISIiArW1tQgLCzNYrqOjA4888gieeuopjZIHoFd+9uzZmDp1qngN91DkvvJbUhu9ry9QVkZm\nG09DsC3TZLw45ObmYv369QCA9evX6ylxAcYYZs2aheTkZMybN09vX61gRwOwefNmpFIGGLuR+8pv\nCmrmYUhtupH7SMcVaWxsxOOPP45Lly4hOjoan3/+ORQKBby8vDB58mRs374d3333HR588EGkpaXd\nNZsCy5Ytw6RJk/DMM8/g+PHj8PLyQkxMDNauXaux+QuQ6cY6hgzh5ujAQODECXEHsxTUjLAaqRV9\ndjYf6QB8VP/556KdmugGrYx1HvffD+zfz7cjIoCzZ8Ub1LjkyljCs6AYN4QnEBCg3a6rk5/5RtIR\n/dixjB7hnYzUI3qKcSMdNKJ3Hs3NQFISV/Jix75xOdON4F5Jj/DOgxKPuC+k6J2LowY1Lmm6oUd4\ngiDcEYWCv/Ly5LcKXFJF7yqR3ghxSEzksg4NlX9OTYIQA7m6WZLXjYchpenmnnt4qjWAx1LXCbVC\niERBAfDVV/x3bmwk042zcYSbpcuZbuT2OEM4Ft2+qVI5rx3uTHk5nwBsbHR2SwiAK3oAuH4dSEiQ\nz5OspIpebo8zhGO57z7+npoKbNzo3La4K4L7KiEPdN0s29u1/wFnI6npZsQIRjZ6JyOl6YZcKx1P\nczMwcybg5QVs2UKmG2fT3MyD+Ak/3eTJwPbt9tXpcu6VFALB+ZB7pftC7pXyQFgN3q8fMHo0j/Vk\nj96TzEZfUlKCxMRExMfHY/ny5T32f/zxx0hPT0daWhrGjBmDkydPGqxHjm5HnoharUZCgvE0dAJ3\n7tyBSqWyKbohJRshPJUtW7in2Y0bPGqrHMzVZhX9nTt3MHfuXJSUlODMmTP45JNPcPbsWb0yQ4cO\nxb59+3Dy5Em89tprKDByZXJ0O/JE1Go1ysuNp6ETeOutt5CcnKwJimUNcnUzIwhHo1AAffrw7cBA\nYMUK57YHsEDRHz58GHFxcYiOjoavry+mT5+OrVu36pUZPXo0AgMDAQBZWVm4fPmy0fqCgmjBlLPJ\nzzedhg4ALl++jOLiYsyePdumx0aKcUN4MrreN/PnO7ctgAWJR6qrqxEZGan5rFQqcejQIaPl161b\nh8mTJxvc16fPYuTnA//7v5SaTCoMpSWzJA3dCy+8gBUrVqClpcXsOQylnCsqoolYR+OIlHOEOAje\nN3IZ6JhV9NY8tu/Zswfvv/8+9gvxOrtRW7uY/vQSYCoNnS5eXl4G5fv1118jLCwMKpXKIkWiq+gF\nhPymhOPoPlj605/+5LzGEHrIbaBjVtEPHjwYVToJXquqqqBUKnuUO3nyJObMmYOSkhIEBQUZrEsO\nF+wJmEpDB8BsGroDBw5g27ZtKC4uxq1bt9DS0oJnnnkGGzZscFSTCcKt0I17I4uIvcwMHR0dbOjQ\noayiooLdvn2bpaenszNnzuiVuXjxIouNjWXff/+90XosOBUhAQBYYWEhY4yxZcuWsQULFpgsX1pa\nyh5++GGT9RHyQExZkFztJyKCMe5Rz9i0abbXI4YszE7G+vj44O2338bEiRORnJyMX/3qV0hKSsLa\ntWuxdu1aAMDrr7+OpqYmPPfcc1CpVBg5cqQDb02EvezcuRMJCQnYvXs3Fi5cCACoqanBlClTDJa3\nxeuGIDwdIc4TwBe0ORNKPOJh0IIp94UWTMkLtZr70atUwO7dtus8l1sZS4lHnI+jFT0lA3cepOjl\nhVghQFxS0YudZouwDkcrekoG7jxI0bsnLhemmBKPuD+0UIog5AclHvEwHD2ip4iVzoNG9O6Jy43o\nCffn5ZeBK1eAGTMomJkjaGxsNBuU7tatW8jKykJGRgaSk5PxyiuvWHU8IS5yCPBHGaYIUaFgZo6l\nsLDQbFC6Pn36YM+ePTh+/DhOnjyJPXv2aFarW3I8IS5y+E9QhilCVMhG71i2bdtmUVA6v7uCaG9v\nx507dzSr1S09nhAPOfwnzIZAEJNevbg/6cWL4iTNJeSH3GJ8uBv19fUWBaXr6urCvffeiwsXLuC5\n555DcnKyVccDhoPVEdZj7X/CEcHqnOJHr1TyTOmE9NCCKfljKihdfn4+mpqaNN8FBwej8W5mcEOy\nuH79OiZOnIjCwkJkZ2cjKCjI6PG6kFzlgxiykHRED/DHmO++k/qsBOE6mApKFx4ebjYonS6BgYGY\nMmUKjh49iuzsbKuPJ9wDSW30Xl58OfDdHCUEQVhJbm4u1q9fDwBYv3498vLyepS5du2axpvm559/\nxs6dO5GRkWHx8YT74RTTTV4esHmzFGcluuNI0w2FP3A8jY2NePzxx3Hp0iVER0fj888/h0KhQE1N\nDQYPHgzGGE6ePImZM2eiq6sLXV1dePrppzH/bpojY8d3h0w38sElQyAAwLRpPIEuIT2OVPQU/sC5\n0IIpeWPrQMglFX1gIFBZSaM9Z+EIRT9nDkN5OXD6NHDtGiiekZMgRS9vdAdCERHA2bOW/UdcbmVs\nUBBw4gQpAHdDWBBy7Rr3qCIlTxA9EfzpAaCuDnj2WenOLamiT04GnnuOVse6Gz/9xN8DA7lHFSl5\nguhJUZH+5xs3pDu3pIp+/35aHeuODBnC369fB+7O+REE0Q2Fgi8aFTh9WrpzSx7UjJbGux8BAfyd\nZEsQphGedv38gAMHpDsvxaMn7KaoiGRLEJZw9CifxzpzRtowMBSP3sOgEAjuC3nduCcu53VDEARB\nSA/FoycIgnBzKB49QRCEmyOpoievDPfE2WnSCIIwjaSTsU1NjLwynIwjJmOFGEYU38a50GSse+Jy\nsW6o4zgfRyn6wEAe3oIyhzkPUvTuCXndELKBVsUShHwhRU+IAs2/EIR8IfdKwm4iIoBNm2hVLEHI\nFcndK2fOlPKMhBTU1ZHZhiDkjOSmm++/p1G9u0FmG4KQN5Ir+itXaNGUu0HBzAhC3kieSlChACoq\nSDE4Cwpq5r6Qe6V74pLulffdR0qeIAhCoKDA8avLJVX0KhXw8cdSnpEgCELeCDmXHRkLzKyiLykp\nQWJiIuLj47F8+XKDZX7/+98jPj4e6enpKCsrM1rX7t3ijeZLS0vFqUjkulyhvsbGRqjVaiQkJGDC\nhAloNjKMiI6ORlpaGlQqFUaOHClqG4wh599OLm2zRH63bt1CVlYWMjIykJycjFdeeUWzb/HixVAq\nlVCpVFCpVCgpKbH1Ekxi7+8lxu/t7DZYcryQNLx/f6CpyTGjepOK/s6dO5g7dy5KSkpw5swZfPLJ\nJzh79qxemeLiYvz44484f/483nvvPTz33HNG6wsKAry8gOef1//ekkcXoUxkJP9hcnJKMWAAcPGi\n/r6ICCA4GFCrLf/BdIVhqC3WPlq99FKppvzMmcDAgUBIiPE26dafn9/zXMbaN3Om4XaZa29hYSHU\najXKy8uRk5ODwsJCg9fh5eWF0tJSlJWV4fDhw+YvXATkokwdXZc99Vkivz59+mDPnj04fvw4Tp48\niT179mD//v0AuFxffPFFlJWVoaysDJMmTbLnMozibCUrhzZYcnxRERAaCrS2Art2OWZU72Nq5+HD\nhxEXF4fo6GgAwPTp07F161YkJSVpymzbtg35+fkAgKysLDQ3N6O+vh7h4eFG6333XeCrr4D2dqCj\nA2hr49sAV4gjR/KbQlERfwIoKAA+/JCX1aWhARg6FPD2Bjo79fft2gWEhfFkvOnpXPkXFQGjRnG/\n79u3td+npQGJifz7mze15yko4EG6vvqK7wO4ct2yRf9ciYnATz/x7TFjgKtXgWPHel73rl3Avffy\nZNp+fly4O3fy8kL7+/QBbt3i2xkZQHQ0UF0NzJvHfwvhMQ/gx1+9yrcTEoDMTH6NumXCwoCxY/UX\nNG3btg177xbIz89Hdna2UWVPE3Lyw1L5+d0dKra3t+POnTsICgrS7CO5ygeFgv93d+xwnKuyyRF9\ndXU1IiMjNZ+VSiWqq6vNlrl8+bLZE1dVAfX1QGOjVskDQFcXcPAgv+hnn+XflZf3VPK65QUl6eWl\nc2He/Jhbt4BDh7T2r7o6HpdF93tBkV+/rj3PgAFATQ0fFd++ra1X9xwCdXX8uI4OoLSUP34B/FFM\nF19fYNAgrT1uxw6gtlbb/qAgrugFamp42R9/5G0vKABOneL7VCp+owIAHx+u8IUFacKjIMDb1H2U\noHsjDg8PR319vcHf1svLC+PHj0dmZib+/ve/GyxDSI+l8uvq6kJGRgbCw8Mxbtw4JCcna/atXr0a\n6enpmDVrllHTHSEdDs+7zEzwxRdfsNmzZ2s+f/TRR2zu3Ll6ZR5++GH23XffaT7n5OSwo0eP9qgL\nPJYtvWTwUigUerIJCgoyKP+amhrGGGNXrlxh6enpbN++fSRXmb8M0dzczLKystiePXsYY4zV19ez\nrq4u1tXVxV599VX261//muQq85e9mBzRDx48GFVVVZrPVVVVUCqVJstcvnwZgwcP7lEXY4xeMnmF\nh4ej7q4tqra2FmFhYQblP3DgQABAaGgo/uu//sugnd7Z1+KJr2HDhqG2thaMMdTU1GDYsGGafYYI\nDAzElClTcOTIEQBAWFgYvLy84OXlhdmzZ5NcXeBlLyYVfWZmJs6fP4/Kykq0t7fjs88+Q25url6Z\n3NxcbNiwAQBw8OBBKBQKk/Z5wvnk5uZi/fr1AID169cjLy+vR5mbN2+itbUVAHDjxg3861//Qmpq\nqqTtJAxjifyuXbumMcn8/PPP2LlzJ1QqFQB+cxfYvHkzydUTYGYoLi5mCQkJLDY2li1dupQxxtia\nNWvYmjVrNGWef/55Fhsby9LS0gyabQh50dDQwHJyclh8fDxTq9WsqamJMcZYdXU1mzx5MmOMsQsX\nLrD09HSWnp7Ohg8frpE94Xwskd+JEyeYSqVi6enpLDU1lb3xxhua459++mmWmprK0tLS2LRp01hd\nXZ1TroOQDrOK3lp27NjBhg0bxuLi4lhhYaHBMr/73e9YXFwcS0tLY8eOHbO5ro0bN7K0tDSWmprK\n7rvvPnbixAm728YYY4cPH2a9evViX375pd317dmzh2VkZLDhw4ezsWPH2lzX1atX2cSJEzWK94MP\nPjBa17PPPsvCwsJYSkqK0TKWyqA7DQ0NbPz48T2UjCE6OztZRkYGe/jhh+2q79KlSyw7O5slJyez\n4afWhYsAAAhvSURBVMOHs7feektvv5h9zpL6nNnvxOxzlsrSWH/68ccfWXBwMPP19WXBwcGssrLS\n4jbv2LGDhYSEsF69erGBAweyjIwMtmPHDr3jjMlsx44dTKlUMl9fXxYSEmK1zKOiolh0dDS75557\n2D333GPw+LNnz7JRo0axe+65h/3lL38xeD0DBw5koaGhRmVhqg5L2mCqn1napwREVfSdnZ0sNjaW\nVVRUsPb2dpaens7OnDmjV2b79u3soYceYowxdvDgQZaVlWVzXQcOHGDNzc2MMX7hxuqytD6h3Lhx\n49iUKVPYF198YVd9TU1NLDk5mVVVVTHGuLK2ta5FixaxhQsXauoJDg5mHR0dBuvbt28fO3bsmFFF\nb6kMDDF//ny2fPlyxhhjhYWFbMGCBUbLvvnmm2zGjBls6tSpdtVXW1vLysrKGGOMtba2soSEBM3v\nI2afs7Q+Z/U7MfscY5bL0lh/GjFihOZ3nTRpksHfwVCbT506xWJjY9m8efPYG2+8YZXMOjs72dCh\nQ1lUVBQrLy9naWlpLDEx0SqZR0VFsZiYGJO/45UrV9i///1v9uqrr+opaeF6fvzxRxYbG8uSkpLY\niRMnrKrD0jYY62eW9ildRA2BoOt37+vrq/G718WY370tdY0ePRqBgYGauky5dVpSH8Ddzh599FGE\nhobafa1FRUV45JFHNBPYAwYMsLmugQMHoqWlBQDQ0tKCkJAQ+PgYXgbxwAMP6PlMd8dSGZg7Nj8/\nH1u6Lyq4y+XLl1FcXIzZs2ebnEyypL6IiAhkZGQAAPz9/ZGUlISamhoA4vY5S+tzVr8Ts88BlsvS\nWH86ceIE3njjDQDA8uXLDa6KN9Tmd955B3FxcVAoFPD29rZYZnV1dTh8+DBCQ0M1q/WfeOIJREVF\nWSXz27dvIyYmxuTvGBoaiszMTPj6+hq8nitXriAuLg7PPPMMiouLrarD0jYY62eW9ildRFX0Yvrd\nW1KXLuvWrcPkyZPtbtvWrVs1q3u9DDnNW1Hf+fPn0djYiHHjxiEzMxMfffSRzXXNmTMHp0+fxqBB\ng5Ceno633nrLaNvMYevaB8ByH+4XXngBK1asgLe36S5maX0ClZWVKCsrQ1ZWltFrsWeth5z7nZh9\nDrD+t+9OR0cHUlJSAAApKSnoMLDYxVCbKysrNd+tXr0a7777LjZs2KDnz2/sWmtqauDv76/Zp1Qq\n0dXVZZHMhTJdXV04fvy4Zn2IORkbqld4F461pg5b2qDbz6zto4CZlbHWYkox6tJ9hGfoOEvrAoA9\ne/bg/fff1yzxtrVt8+bNQ2FhoSYsqKmRqCX1dXR04NixY/jmm29w8+ZNjB49GqNGjUJ8fLzVdS1d\nuhQZGRkoLS3FhQsXoFarceLECfTvvirLQkzJQK1Wa9wvdVmyZEmPYwy1/euvv0ZYWBhUKhVKS0tx\n8OBBg54dltYn0NbWhkcffRRvvfUW/P39e7TbFJb0OWvqA6Tvd7b0ucjISPz5z39G79699cpZ+tsL\nfaGjowOVlZUaOXY/3tgN3VSbn3vuOfy///f/sHHjRqxatQovvfQS1q1bp9lv6v9nCcaO//Of/4zD\nhw9j6dKlUKvVmDp1qsV1CtdjTT8xxJIlS3Do0CGL2tC9n9lyblEVvZh+95bUBQAnT57EnDlzUFJS\nYtJcYUl9R48exfTp0wFw97QdO3bA19e3h0uppfVFRkZiwIAB6Nu3L/r27YsHH3wQJ06c6KHoLanr\nwIEDePXVVwEAsbGxiImJwblz55CZmWn0mo1hTgY7d+40eqzggx8REWHUB//AgQPYtm0biouLcevW\nLdy8eRMqlUrjhmttfQBXYI888gieeuopPXdCMfucpfUBzul3tvS5GTNmYNKkSXj00Ud7tM2S317o\nC5WVlZg6dSpOCUuzAfj6+uLkyZNIS0vD8ePHDZooDLV56NChuHDhguZ8ly9fRk5ODrZv3270uMuX\nL0OpVKKjowNtbW2afVVVVfD29rZK5qmpqfjyyy8160MOHTqEsWPH9mi7IYR6hXdBBsb6iTFSUlLw\nxRdfmG2DoX5maR/Vw6QF30o6OjrY0KFDWUVFBbt9+7bZSZbvv//e6ESWJXVdvHiRxcbGsu+//16U\ntukyc+ZMk94PltR39uxZlpOTwzo7O9mNGzdYSkoKO336tE11vfDCC2zx4sWMMcbq6urY4MGDWUND\ng9H2VVRUWDQZa0oGhpg/f75mln/ZsmUmJ2MZY6y0tNSk140l9XV1dbGnn36azZs3r8c+MfucpfU5\nq9+J2ecYs06WhvrTiBEj2KRJkxhjjE2cONHg72qozSdPnmRDhw5lhw4d0ny3YMEC9sQTT2iOMyaz\njo4OFhMTw4YMGcLOnTvHUlNTzU7G6h5/48YN1tjYyIYOHcpOnz7NRo0axYYOHWpUJosWLdKbSBWu\n5/z582zo0KEsKSmJHT9+3KRcu9dhaRuM9TNr+xRjDnCvFNPv3lxds2bNYsHBwSwjI4NlZGSwESNG\n2N02AXOK3tL6VqxYwZKTk1lKSkoPt0Br6rp69Sp7+OGHWVpaGktJSWEff/yx0bqmT5/OBg4cyHx9\nfZlSqWTr1q0Tbe2DJT7cupSWlpr0urGkvm+//ZZ5eXmx9PR0jax1XfHEXush534nZp+zVJZCf+rd\nuzdTKpXs/fffZ4xx98qgoKAe7pXdjzfU5uLiYhYQEMB69+7NIiIi2LRp09gbb7xhkcyKi4vZ4MGD\nNee1RubC+pCYmBjWu3dvo8fX1tYypVLJAgICmEKhYJGRkay1tVXvegYOHMgGDBhgVBbG6rC0Dab6\nmaHf1BSSpRIkCIIgnIPkqQQJgiAIaSFFTxAE4eaQoicIgnBzSNETBEG4OaToCYIg3BxS9ARBEG7O\n/wfVl6MnaN4sqQAAAABJRU5ErkJggg==\n",
       "text": [
        "<matplotlib.figure.Figure at 0x351e710>"
       ]
      }
     ],
     "prompt_number": 5
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "References"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "[1]E. J. Candes and T. Tao, \u201cNear-optimal signal recovery from random projections: Universal encoding strategies?,\u201d Information Theory, IEEE Transactions on, vol. 52, no. 12, pp. 5406\u20135425, 2006.\n",
      "\n",
      "[2]S. Ganguli and H. Sompolinsky, \u201cCompressed sensing, sparsity, and dimensionality in neuronal information processing and data analysis,\u201d Annual review of neuroscience, vol. 35, pp. 485\u2013508, 2012.\n",
      "\n",
      "[3]G. Isely, C. J. Hillar, and F. T. Sommer, \u201cDeciphering subsampled data: adaptive compressive sampling as a principle of brain communication,\u201d arXiv preprint arXiv:1011.0241, 2010.\n"
     ]
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Other Useful References"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "[1]E. J. Cand\u00e8s and M. B. Wakin, \u201cAn introduction to compressive sampling,\u201d Signal Processing Magazine, IEEE, vol. 25, no. 2, pp. 21\u201330, 2008.\n",
      "\n",
      "[2]M. Lustig, D. Donoho, and J. M. Pauly, \u201cSparse MRI: The application of compressed sensing for rapid MR imaging,\u201d Magnetic resonance in medicine, vol. 58, no. 6, pp. 1182\u20131195, 2007.\n",
      "\n",
      "[3]<a href='http://dsp.rice.edu/cscamera'>The single pixel camera</a>\n"
     ]
    }
   ],
   "metadata": {}
  }
 ]
}