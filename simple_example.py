import numpy as np
from matplotlib.pyplot import figure, title

class SimpleIC():

    def __init__(self, ndim=3, k=1, noisy=True):

        self.ndim = ndim
        self.k = k if k >= 1 else int(k * self.ndim)
        self.noisy = noisy

        self.B = np.random.normal(size=(self.ndim, self.ndim))
        self.B /= (self.B ** 2).sum() ** .5

        self.generate_data()

    def generate_data(self, npoints=200):
        self.s = np.zeros((self.ndim, npoints))

        for ii in xrange(npoints):
            inds = np.random.permutation(self.ndim)
            vals = np.random.uniform(0, 1, self.ndim)
            self.s[inds[:self.k], ii] = vals[inds[:self.k]]
            if self.noisy:
                self.s[inds[self.k:], ii] = .01 * vals[inds[self.k:]]

        self.data = np.dot(self.B, self.s)

    def project_data(self, proj_type="random", m=2):

        if proj_type == "random":
            P = np.random.normal(0, 1, size=(m, self.ndim))
            P /= (P ** 2).sum() ** .5

        elif proj_type == "pc":
            from sklearn.decomposition import PCA
            pca = PCA(n_components=m)
            pca.fit(self.data.T)
            P = pca.components_

        elif proj_type == "ic":

            Binv = np.linalg.pinv(self.B)
            inds = np.random.choice(self.ndim, m, replace=True)
            P = Binv[inds]

        self.proj_data = np.dot(P, self.data)
        return self.proj_data
            

    def plot_pcs(self, ax=None):

        proj_data = self.project_data("pc")
        plot_2d(proj_data, ax)

    def plot_ics(self, ax=None):

        proj_data = self.project_data("ic")
        plot_2d(proj_data, ax)

    def plot_random(self, ax=None):

        proj_data = self.project_data("random")
        plot_2d(proj_data, ax)

    def plot_all(self):

        fig = figure()

        ax = fig.add_subplot(131)
        print "Plotting 2 independent components"
        self.plot_ics(ax)
        ax.set_title("Two ICs")

        ax = fig.add_subplot(132)
        print "Plotting 2 principal components"
        self.plot_pcs(ax)
        ax.set_title("Two PCs")

        ax = fig.add_subplot(133)
        print "Plotting 2 random projections"
        self.plot_random(ax)
        ax.set_title("2 RPs")

    def plot_dists(self, resolution=0.05):

        fig = figure()

        mean_dists = np.zeros((3, int(1. / resolution)))
        norm_dist = compute_dists(self.data)[0]
        for ii, pc in enumerate(np.arange(resolution, 1, resolution)):
            m = int(pc * self.ndim)
            mean_dists[0, ii] = compute_dists(self.project_data("ic", m))[0]
            mean_dists[1, ii] = compute_dists(self.project_data("pc", m))[0]
            mean_dists[2, ii] = compute_dists(self.project_data("random", m))[0]
            
        return mean_dists, norm_dist

def plot_2d(data, ax=None):

    if ax is None:
        fig = figure()
        ax = fig.add_axes()
    
    ax.plot(data[0], data[1], '.', color='b')

def compute_dists(data):
    from scipy.spatial.distance import pdist
    
    npoints = data.shape[1]
    dists = pdist(data.T)

    upper_inds = np.triu_indices(npoints, 1)
    return np.mean(dists[upper_inds[0]][:, upper_inds[1]]), dists
    